import numpy as np
import matplotlib.pyplot as plt


def main():
    file = open('ex2data1.txt')
    x = []
    y = []
    t = ([float(0)],[float(0)], [float(0)])
    for a in file:
        a = a.split(sep=',')
        x.append([float(1),float(a[0]),float(a[1])])
        y.append([float(a[2])])
    t = np.matrix(t)
    x = np.matrix(x)
    y = np.matrix(y)

    descent(x,y,t)




def J(x, y, t, H):
    m=len(x)


    logH = np.log(H + 1e-12)
    A = -1 * (logH * y)

    b = np.log(1 - H + 1e-12)
    B = -1 * np.sum((b))

    C = b * y
    return (A+B+C)/m

def sigmoid(x):
    r=1/(1+np.exp(-1*x))
    return r

def descent(x,y,t):
    a=0.001
    m=len(x)
    for i in range(500000):
        H = sigmoid(np.transpose(t) * np.transpose(x))
        t = t - a * (np.transpose(x) * (np.transpose(H)-y))/m
        print(J(x,y,t,H))
    plotgraph(x,t,y)


def plotgraph(x,t,y):
    m=len(x)
    x0 = []
    y0 = []
    x1 = []
    y1 = []
    for i in range(m):
        if (y[i] == 0):
            x0.append(x[i, 1])
            y0.append(x[i, 2])
        if (y[i] == 1):
            x1.append(x[i, 1])
            y1.append(x[i, 2])

    plt.scatter(x0, y0, color='red')
    plt.scatter(x1, y1, color='blue')
    r=np.arange(20,100)
    X= -(t[0] + t[1]*r)/t[2]
    plt.plot(r,np.transpose(X))
    plt.show()




main()
